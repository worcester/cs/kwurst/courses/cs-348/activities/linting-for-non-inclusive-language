# Linting for Non-Inclusive Language

## Introduction

Computing, as a field, has a continuing problem with inclusion and
diversity. Many people with identities that
not <!--alex disable white--> white,<!--alex enable white--> cis-gender,
are <!--alex disable heterosexual-->heterosexual,<!--alex enable heterosexual-->
neurotypical, non-disabled, Christian, young,
and <!--alex disable gals-man-->man<!--alex enable gals-man--> may not
feel welcome and included in the field.

This is a problem because many people who want to be in the field, who
would bring diverse perspectives, skills, and ideas to projects either do
not join the field, or leave due to not feeling like they belong or are
valued. Because of this, the tools, products, and data we produce are not
as good or as useful as they could be for a variety of people.

Often one of the first places someone encounters a feeling of not belonging
or not being welcomed is in documentation for projects and tools that are
used and built by practitioners in the community. By being thoughtful and
careful in how we write we can make sure we do not turn people away before
they even join our communities.

There are more and more guides being produced to help us write in inclusive
ways, but we are not as used to re-reading and editing our work for inclusive
language as we are for spelling and grammar.

In the same way that spell-checkers and grammar-checkers help us in our
writing, there now tools that can help with writing in an inclusive way.
And, <!--alex disable just-->just<!--alex enable just--> as we know to
sometimes ignore the warnings of spelling and
grammar checkers because they sometimes misinterpret our writing, we need
to be able to evaluate the warnings that inclusive writing tools produce, to determine
if their suggestions are valid.

## Content Learning Objectives

By the end of this activity, participants will be able to...

* Review written language to spot non-inclusive language.
* Evaluate whether language flagged by an automated tool is problematic.
* Mark flagged language as non-problematic to be ignored by the tool.
* Explain to an author, in a polite, non-threatening way, why something
    they have written may need to be rewritten, and how it could be.

## Process Skill Goals

During the activity, students should make progress toward:

* Teamwork - Having a discussion with teammates about reasons why language
may not be inclusive.

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder/Technician |
Reflector |

## Instructions

1. Fork this activity to your team's space in GitLab.
2. Have your Recorder/Technician clone it to their computer.
3. Open it in Gitpod. **It may take a little time for the Alex linter to be installed. Watch the terminal output to see when it completes**
4. We are going to lint the LibreFoodPantry.org website. Get the source code:

    ```bash
    git clone https://gitlab.com/LibreFoodPantry/librefoodpantry.gitlab.io.git text-to-lint
    ```

## Model 1 - Running Alex

You will be using the Alex linter, which is designed to "catch insensitive,
inconsiderate writing".

> *Whether your own or someone else’s writing, **alex** helps you find
gender favouring, polarising, race related, religion inconsiderate, or
other **unequal** phrasing.*
>
> [https://alexjs.com/](https://alexjs.com/)

1. Visit the [Alex website](https://alexjs.com/) and  look through the main page together. Note anything interesting you find.
2. Run the Alex linter on the issue tracker documentation file:
`alex text-to-lint/LibreFoodPantry/docs/issue-tracker.md`
3. Record the output here.
4. What is the file that it is linting? Where did you find that in the
output?
5. Find the line in the text-to-lint/README.md file. How did you know which
line?
6. Record the line here:
7. Discuss why the word was flagged.
    1. What makes this word potentially problematic?
    2. Is the use of the word problematic here?
8. Try running alex again using the `--why` flag. Does the explanation help?
Why would you consider not using the word?

## Model 2 - Full Output

1. Run Alex on the full `text-to-lint` directory.
2. Review the output.
3. Choose 5 flagged word instances that you would like to investigate
further. Record the warning messages for these below. Make sure you include
which file they are in.

    1.
    2.
    3.
    4.
    5.

## Model 3 - Resources

1. Go to the [Alex GitHub page](https://github.com/get-alex/alex)
2. Find the files for the full rules under the `Checks` section. Look at
at the `retext-equality` rules file. List the four column headers for the
table.
3. Search for one of the words you noticed was flagged. Compare the warning message to the contents of the table for that line. Can you find the `id`
in the warning? You may want to use that `id` in a later step.
4. Find the `Further reading` section take a brief look at one or two of the
guides to see what they can help you with.

## Model 4 - Deciding how to fix a warning

Warnings may flag something that needs to be fixed, or may be a "false positive" - a use of the word that is actually OK in context.

Go back to your list of warnings from Model 2. For each warning do the following:

1. Find the possible problem in the file.
2. Discuss whether this is really a problem that needs to be fixed, or
a false positive that needs to be ignored. Use the `--why` flag, the
`retext-equality` rules listing, or some of the `Further reading` resources to help you make your decision. Record your decision, and your reasoning:

    1.
    2.
    3.
    4.
    5.

From your list of 5 warnings above, choose:

1. A warning that represents a problem that should be re-written.
    1. Record it below:
    2. How would you suggest fixing it? Do that by editing the file.
    3. If it was in someone else's writing, how would you write to them
    suggesting to fix it? Do so in a way that does not sound like you
    are attacking them.
2. A warning that is a false-positive.
    1. Record it below:
    2. Look at the `Control` section on the Alex GitHub page, specifically
    the section on using HTML comments in Markdown to ignore warnings. You can also see examples of this in the source code of the  [introductory paragraph of this file itself](#introduction).
    3. Write the HTML comment you would add to the file. (note this is
    where you need the rule `id` from the warning):
    4. Edit the file to use this. (Note that you should probably turn the
    warning back on after the offending line.)
3. Run `alex` again on the file(s) you edited. Did the warnings you fixed
go away?

## Identity-Inclusive Activity Post-Survey

The Linting for Non-Inclusive Language activity that
you <!--alex disable just-->just<!--alex enable just--> completed was
developed as part of my work as a Cultural Competency in Computing
(3C) <!--alex disable gal-guy-->Fellow.<!--alex enable gal-guy-->
Please complete this survey to help this program measure the impact of this and other activities by other 3C Fellows.

[https://duke.qualtrics.com/jfe/form/SV_50ziImidtynLItE](https://duke.qualtrics.com/jfe/form/SV_50ziImidtynLItE)

When completing this enter "Worcester State University", "Linting for Non-Inclusive Language", "Karl Wurst" when asked on the second page.

Your participation in this survey is optional, but very much appreciated. I will not be told who completed the survey, or what they answered.

&copy; 2024 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA
